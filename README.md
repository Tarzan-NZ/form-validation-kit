# Form Validation Kit

This script can be used to validate forms. It is easy to extend the `validateField` function to validate all kinds of fields

## Usage
```html
<script src="form-validation-kit.js"></script>
<script>
document.addEventListener("DOMContentLoaded", function() {
	addFormValidation(document.querySelector("#contact-form"));
});
</script>
```

See `contact-form.html` for a working example.

## Supported Fields
* `<input type="text">`
* `<input type="email">` -- if not blank, must be an email address
* `<input type="checkbox">`
* `<textarea>`
* `<select>` -- invalid options have a blank value, i.e. `value=""`

## Validation types:
* `required` -- add the required attribute to a field to ensure it is not empty, or for checkboxes, is checked, or for select boxes, has option with a non-blank value.
* `class="fv-minlength-n"` -- fields with this class will need to have *n* or more chars, e.g. `<input type="text" class="fv-minlength-2">` for two or more chars.

## Error Messages
Every individual non-button field must have a page-unique `id=""` attribute, along with a matching element with the same id followed by `-error`. 

These error elements are **required** for non-button fields even if they are not being validated.

Example `username` and `username-error`:

```html
<input id="username" name="username" type="text" required class="fv-minlength-2">
<div id="username-error" class="danger"></div>
```

### Debugging

If your form is submitting without properly validating, you probably have errors. Open your browser's JavaScript console and enable the 'persist' or 'preserve log' option, then try submitting your form. You should see any errors you might have. 

With this preserve option enabled, be sure to clear your console of errors when necessary so you don't get confused by 'stale' errors.
